
from gnc.client import GNC, SERVER, LIST_COMMANDS, SEP
from gns.library import pattern_value, fill_pattern, context_split

try:
    from gnch_settings import LIST_RECV
except:
    from .gnch_settings import LIST_RECV

import asyncio
import asyncssh
import sys

# |Session factory:
from multiprocessing import Process, Pool, Pipe, Queue


class GPSNetworkChannel(object):
    # Here we have all variables from GNC and GNCSocket
    COMMANDS = LIST_COMMANDS

    def __init__(self, flag_gnc, flag_gus, queue_recv, queue_send):
        super(GPSNetworkChannel,  self).__init__()
        # Socket as server: mode=server
        self.gnc = GNC()
        # RUN GNC Activation
        self.loop = asyncio.get_event_loop()
        self.queue_recv = queue_recv
        self.queue_send = queue_send
        # Pipes
        self.parent_gnc = flag_gnc
        self.child_gus = flag_gus
        self.parent_data, self.child_data = Pipe()

    def mainloop(self):
        pool = Pool(processes=3)
        pool.apply_async(self.gnc_connect)
        pool.apply_async(self.gns2connect)
        pool.apply_async(self.exchange_data)
        pool.close()
        pool.join()

    def gns2connect(self):
        gnc_loop = asyncio.new_event_loop()
        gnc_loop.run_until_complete(self.run_gnch())

    async def active_sock(self):
        print("Activando socket")
        await self.loop.run_until_complete(self.gus.accept())
        print("Socket ok")

    # extends data_received

    def exchange_data(self):
        print(self.gnc)
        print("Recibiendo data GNC")
        while True:
            print("Aceptando nueva conexión")
            p_gus = Process(target=self.chan2socket)
            p_gnc = Process(target=self.chan2gnc)
            p_gus.start()
            p_gnc.start()
            p_gus.join()
            p_gnc.join()

    async def chan2socket(self):
        print("chan2Socket")
        gus_status = self.child_gus.recv()
        self.parent_gnc.send(self.status)
        try:
            if self.gnc.on_session:
                print("Mensajes intercambiados:")
                try:
                    if len(self.gus.conns) >= 1:
                        # receive from GNS
                        msg2 = self.gnc._input
                        # send to socket
                        if not msg2 == '':
                            # print(msg2)
                            msg2socket = self.gns2socket(msg2)
                            # print("A enviar")
                            # print(msg2socket)
                            if gus_status == 'ON':
                                await self.queue_send.put(
                                    msg2socket)
                        # read from socket
                        msg = await self.queue_recv.get()
                        # print("Mensaje de socket")
                        print(msg)
                        # send to GNS
                        if not msg == '':
                            self._chan.write(self.socket2gns(msg))
                except KeyboardInterrupt as k:
                    self.close()
            elif not self.on_session:
                print("No hay conexión a red GNS")
            elif self.gus.status == 'OFF':
                print("No hay conexión a red socket de cliente")
        except:
            print("GNC aún no conecta")

    async def chan2gnc(self):
        print("chan2gnc")
        gus_status = self.child_gus.recv()
        self.parent_gnc.send(self.status)
        try:
            if self.gnc.on_session:
                print("Mensajes intercambiados:")
                try:
                    if len(self.gus.conns) >= 1:
                        # receive from GNS
                        msg = await self.queue_recv.recv()
                        # send to socket
                        if not msg == '':
                            # print(msg2)
                            msg2socket = self.socket2gns(msg)
                            # print("A enviar")
                            # print(msg2socket)
                            if self.status == 'ON':
                                await self._chan.write(msg2socket)
                                # read from socket
                except KeyboardInterrupt as k:
                    self.close()
            elif not self.on_session:
                print("No hay conexión a red GNC")
            elif self.gus.status == 'OFF':
                print("No hay conexión a red de cliente GNS")
        except:
            print("GNSocket aún no conecta")

    def close(self):
        self.gnc.close()
        sys.exit()

    def socket2gns(self, msg):
        # Socket ----> GNS Code
        final_code = ""
        msg_list = context_split(msg)
        msg_name = msg_list[0]
        msg_content = msg[msg.find(SEP) + 2:len(msg) - 1]
        msg_list = context_split(msg_content)
        var_list = []
        if msg_name in LIST_RECV.keys() and msg_name in \
           type(self).COMMANDS.keys():
            try:
                var_list.append(
                    pattern_value("[IDX]", self.idx),
                )
                sock_struct = LIST_RECV[msg_name]["code"]
                sock_struct_content = sock_struct[
                    sock_struct.find(SEP) + 2:len(sock_struct) - 1]
                sock_struct_list = context_split(sock_struct_content)
                gns_struct = type(self).COMMAND[msg_name]["code"]
                # Optimizar: se puede generar esto al ppio:_>:
                gns_struct_list = context_split(gns_struct)
                if msg_name in type(self).COMMANDS.keys():
                    cid_index = sock_struct_list.index('[CLIENTID]')
                    var_list.append(
                        pattern_value('[CLIENTID]', msg_list[cid_index]))
                    if msg_name == 'CLNTPSSWD':
                        pw_index = sock_struct_list.index('[PASSWD]')
                        var_list.append(
                            pattern_value('[PASSWD]', msg_list[pw_index]))
                    elif msg_name == 'SENDMSG1':
                        dest_index = sock_struct_list.index('[PASSWD]')
                        var_list.append(
                            pattern_value('[CLIENTDEST]', msg_list[dest_index]))
                        msg_index = sock_struct_list.index('[MSG_DATA]')
                        var_list.append(
                            pattern_value('[MSG_DATA]', msg_list[msg_index]))
                    elif msg_name == 'SENDMSGGRP' or msg_name == 'SENDMSGALL' or msg_name == 'INFO':
                        msg_index = sock_struct_list.index('[MSG_DATA]')
                        var_list.append(
                            pattern_value('[MSG_DATA]', msg_list[msg_index]))
                        if msg_name == 'SENDMSGGRP':
                            gr_index = sock_struct_list.index('[GRP_LST]')
                            var_list.append(pattern_value(
                                '[GRP_LST]', msg_list[gr_index]))

                    final_code = fill_pattern(var_list, gns_struct_list)
            except KeyError:
                print("No existe ese nombre de mensaje")
        return final_code
        # Pasar mensaje de gns a socket

    def gns2socket(self, msg):
        # GNS Answer ----> Socket
        # No trae MSG_DATA
        MSG_SOCK = ''
        gns_msg = context_split(msg)
        msg_type = gns_msg[0]
        # print("Server a socket:")
        # print(msg_type)
        # print(LIST_RECV.keys())
        assert msg_type in LIST_COMMANDS.keys(), \
            "No es un mensaje de GNS " + msg
        # assert gns_msg[1] == self.idx, "No concuerda IDX"
        # BUSCAR [CLIENTID] en msg2
        estructura = context_split(LIST_COMMANDS[msg_type]['answer'])
        print(estructura)
        var_list = []
        sock_struct = LIST_RECV[msg_type]["code"]
        try:
            name = '[CLIENTID]'
            cid_index = estructura.index(name)
            client_id = gns_msg[cid_index]
            var_list.append(
                pattern_value(name, client_id)
            )
            if msg_type == 'CLNTPSSWD':
                name = '[PSSWD]'
                pw_index = estructura.index(name)
                pwd = gns_msg[pw_index]
                var_list.append(pattern_value(name, pwd))
            elif msg_type == 'SESSIONLST':
                name = '[CLIENTID]'
                sessions_index = estructura.index(name)
                sessions = gns_msg[sessions_index]
                var_list.append(pattern_value(name, sessions))
                # line 302 gns
            elif msg_type == 'COMMANDLST':
                name = '[CLIENTID]'
                commands_index = estructura.index(name)
                commands = gns_msg[commands_index]
                var_list.append(pattern_value(name, commands))
            elif msg_type == 'SENDMSG1' or msg_type == 'SENDMSGALL':
                name = '[CLIENTDEST]'
                destid_index = estructura.index(name)
                msg_data = gns_msg[destid_index]
                var_list.append(pattern_value(name, msg_data))
                name = '[MSG_DATA]'
                destid_index = estructura.index(name)
                dest_id = gns_msg[destid_index]
                var_list.append(pattern_value(name, dest_id))
            elif msg_type == 'SENDMSGALL':
                name = '[MSG_DATA]'
                msg_index = estructura.index(name)
                msg_data = gns_msg[msg_index]
                var_list.append(pattern_value(name, msg_data))
            elif msg_type == 'INFO':
                name = '[MSG_DATA]'
                msg_index = estructura.index(name)
                msg_data = gns_msg[msg_index]
                var_list.append(pattern_value(name, msg_data))

            MSG_SOCK = fill_pattern(var_list, sock_struct)
            return MSG_SOCK
        except ValueError:
            print("ID client no existe o hubo otro problema")

            # Si trate MSG_DATA
    async def run_gnch(self):
        async with asyncssh.connect(SERVER['ip'], port=SERVER['port']) as conn:
            self.chan, self.session = await conn.create_session(GNCh, '')
            self.gnc = self.chan
            await self.chan.wait_closed()

    def gnc_connect(self, loop):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(self.run_gnch())


GNCh = GPSNetworkChannel
