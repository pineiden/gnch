from gns.conf.settings import LIST_COMMANDS

LIST_RECV = dict()
# print(LIST_COMMANDS.keys())
# From socket
for k in LIST_COMMANDS.keys():
    if k == 'INFO':
        LIST_RECV[k] = dict(code=k + "|[[CLIENTID]|[MSG_DATA]]")
    elif k == 'SYNCCLIENT':
        LIST_RECV[k] = dict(code=k + "|[[CLIENTID]]")
    elif k == 'CLNTPSSWD':
        LIST_RECV[k] = dict(code=k + "|[[CLIENTID]|[PASSWD]]")
    elif k == 'SESSIONLST':
        LIST_RECV[k] = dict(code=k + "|[[CLIENTID]|RCV|LST|[SESSIONLST]]")
    elif k == 'COMMANDLST':
        LIST_RECV[k] = dict(code=k + "|[[CLIENTID]|RCV|CMD|[COMANDLST]]")
    elif k == 'SENDMSG1':
        LIST_RECV[k] = dict(
            code=k + "|[[CLIENTID]|[CLIENTDEST]|RCV|MSG|[MSG_DATA]]")
    elif k == "SENDMSGGRP":
        LIST_RECV[k] = dict(
            code=k + "|[[CLIENTID]|[GRP_LST]|RCV|MSG|[MSG_DATA]]")
    elif k == "SENDMSGALL":
        LIST_RECV[k] = dict(code=k + "|[[CLIENTID]|ALL|RCV|MSG|[MSG_DATA]]")
    else:
        LIST_RECV[k] = dict(code=LIST_COMMANDS[k]["code"])

# for k in LIST_RECV:
#     print(LIST_RECV[k])
