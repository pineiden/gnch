import sys
import asyncio
import asyncssh
from gnce import GNCh

if __name__ == "__main__":
    gnch = GNCh()
    try:
        gnch.mainloop()
    except (OSError, asyncssh.Error) as exc:
        gnch.close()
        sys.exit('SSH connection failed: ' + str(exc))
