import sys
import asyncio
from multiprocessing import Process, Pool, Pipe, Queue
from gus.gnc_socket import GNCSocket


class ServerSocket(object):

    def __init__(self, flag_gnc, flag_gus, queue_recv, queue_send):
        self.gus = GNCSocket(mode='server')
        # Queues
        self.queue_recv = queue_recv
        self.queue_send = queue_send
        # Pipes
        self.parent_gus = flag_gnc
        self.child_gnc = flag_gus
        self.loop = asyncio.get_event_loop()

    async def gs_init(self):
        await self.gus.accept()
        await self.gus.send_msg("\"Welcome to socket\"")

    def data_exchange(self):
        while True:
            self.loop.run_until_complete(self.gs_init())
            print("Aceptando nueva conexión")
            pool = Pool(processes=2)
            pool.apply_async(self.data2gnc)
            pool.apply_async(self.data2sock)
            pool.close()
            pool.join()

    def data2gnc(self):
        print("data2gnc")
        gnc_loop = asyncio.new_event_loop()
        gnc_loop.run_until_complete(self.data_exchange2gnc())

    def data2sock(self):
        print("data2sock")
        gus_loop = asyncio.new_event_loop()
        gus_loop.run_until_complete(self.data_exchange2gnc())

    async def data_exchange2gnc(self):
        gs = self.gus
        while True:
            self.parent_gus.send(gs.status)
            gnc_status = self.child_gnc.recv()
            try:
                await gs.recv_msg()
                # time.sleep(5)
                # if not datagram:
                # break
                if not gs.msg_r == '':
                    print("Datagram :")
                    print(gs.msg_r)
                    if gnc_status == 'ON':
                        await self.queue_send.put(gs.msg_r)
                    print("-" * 20)
                if "DONE" == gs.msg_r:
                    break
            except ConnectionResetError:
                print("Conexión cerrada por cliente")
                break
            except ConnectionAbortedError:
                print("Conexión cancelada por cliente")
                break
            except ConnectionError:
                print("Error de conexión")
                break

    async def data_exchange2sock(self):
        gs = self.gus
        while True:
            self.parent_gus.send(gs.status)
            try:
                data = await self.queue_recv.get()
                # time.sleep(5)
                # if not datagram:
                # break
                if not gs.msg_r == '':
                    print("Datagram :")
                    print(gs.msg_r)
                    if gs.status == 'ON':
                        await gs.send_msg(data)
                    print("-" * 20)
                if "DONE" == gs.msg_r:
                    break
            except ConnectionResetError:
                print("Conexión cerrada por cliente")
                break
            except ConnectionAbortedError:
                print("Conexión cancelada por cliente")
                break
            except ConnectionError:
                print("Error de conexión")
                break
